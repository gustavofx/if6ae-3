/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import javax.inject.Named;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Gustavo
 */
@Named(value = "registroBean")
@ManagedBean
@ApplicationScoped
public class RegistroBean {

    /**
     * Creates a new instance of RegistroBean
     */
    
    private final ArrayList CandidatosList = new ArrayList();;
    
    public RegistroBean() {
    }
    
    public void addCanditato(Candidato c){
        CandidatosList.add(c);
    }

    public ArrayList getCandidatosList() {
        return CandidatosList;
    }
    
    
}
